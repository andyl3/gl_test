# Interwatch

Interwatch is an interactive supervisor for file watcher programs.

Interwatch is a pre-production prototype, written as an Elixir Script. 

## The File Watcher Landscape

There are several excellent file watcher programs.  These are typically used
during development to run tests automatically every time a source or test file
is saved.

### Independent vs Bundled

Many file watcher programs are *bundled* as a language-specific project
dependency. (eg guard [ruby], mix_test_watch [elixir], jest [javascript])

Other file watcher programs run *independent* of project and language. (eg
watchexec, entr, fswatch) Independent watchers can be used with any any
language, giving developers flexibity to use their favorite tool in a given
project.

### Interactive vs Static

Most file watchers are *static* - they run the same test command every time a
file changes.

Some file watcher programs are *interactive*, and allow you to dynamically
change which tests should be run with a few keystrokes.  For example, you may
easily switch between running all tests, stale tests, or failed tests.  Or you
can run only the tests whose filenames contain a substring.

Until now, interactive file watcher programs are bundled as a project
dependency (eg mix_test_interactive [elixir], jest interactive [javacript]).  

### Independent, Interactive

Interwatch is an interactive supervisor that works with any file watcher
program.  Interwatch can be used independently across any language or project.

```mermaid
graph TD;
    Interwatch-->InteractiveRepl;
    Interwatch-->YourWatcherProgram;
```

Interwatch supervises two processes:

- a REPL that takes interactive commands from the terminal
- your watcher program - like `fswatch`, `entr` or `watchexec`

When the repl takes commands from the terminal, it restarts your watcher
program with updated options.

## Installing 

TBD 

## Using 

TBD 

## Configuring

TBD 

## Contributing

Issues and PRs welcome!

It would be nice if Interwatch was available as a standalone executable, to
eliminate the need for the Elixir runtime.  Experienced Rust developers - let's
talk!

